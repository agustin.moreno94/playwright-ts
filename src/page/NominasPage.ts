import { Locators } from '../locators/locators';
import { BasePage } from './basePage';
import { Credentials } from '../../config/config';

export class NominasPage extends BasePage {
    constructor(page: any, context: any) {
        super(page, context);
    }

    credentials = Credentials.getCredentials();

    //ir a la web
    async irAPage(): Promise<void> {
        await this.gotoPage(this.credentials.URL,80000);
    }

    // Iniciar sesion en OBE
    async login(): Promise<void> {
        await this.fillLocator(Locators.UserNameLocator, this.credentials.UserName);
        await this.fillLocator(Locators.PassLocator, this.credentials.Password);
        await this.clickElement(Locators.SubmitButton);
    }

    // Alta Cuenta Sueldo
    async AltaCuentaSueldo(): Promise<void> {
        await this.clickElement(Locators.PagosYtransf);
        await this.page.getByText('Alta de Cuenta Sueldo', { exact: true }).click();
        await this.page.getByLabel('Close Tour').click();
    }

    // seleccionar sucursal
    async selecSucursal(suc: string): Promise<void> {
        await this.page.locator(Locators.SelSucursal).selectOption(suc);
    }
}
